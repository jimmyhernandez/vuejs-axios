import Vue from 'vue'
import Vuex from 'vuex'

import axios from './axios-auth';
import globalAxios from 'axios';

import router from './router';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken: null,
    userId: null,
    user: null
  },
  mutations: {
    'AUTH_USER'(state, userData) {
      state.idToken = userData.token;
      state.userId = userData.userId;
    },
    'STORE_USER'(state,user) {
      state.user = user;
    },
    'CLEAR_AUTH_DATA'(state) {
      state.idToken = null;
      state.userId = null;
      state.user = null;
    }
  },
  actions: {
    setLogoutTimer({commit}, expiration) {
      setTimeout(() => {
        commit('CLEAR_AUTH_DATA');
      },expiration);
    },
    signup({commit, dispatch},authData) {
      axios.post('/signupNewUser?key=AIzaSyBfHM4l8GKypcpGt6FAnfNMf_BVpxK4HF4',{email: authData.email, password: authData.password,returnSecureToken: true})
          .then(res => {
            commit('AUTH_USER',{
              token: res.data.idToken,
              userId: res.data.localId
            });

            const now = new Date();
            const expirationDate = new Date(now.getTime() + (res.data.expiresIn * 1000)); 
            localStorage.setItem('token',res.data.idToken);
            localStorage.setItem('expirationDate',expirationDate);
            localStorage.setItem('userId',res.data.userId);

            dispatch('storeUser',authData);
            dispatch('setLogoutTimer',res.data.expiresIn);
          })
          .catch(error => console.log(error));
    },
    login({commit,dispatch},authData) {
      axios.post('/verifyPassword?key=AIzaSyBfHM4l8GKypcpGt6FAnfNMf_BVpxK4HF4',{email: authData.email, password: authData.password,returnSecureToken: true})
          .then(res => {
            commit('AUTH_USER',{
            token: res.data.idToken,
            userId: res.data.localId
          });
          const now = new Date();
          const expirationDate = new Date(now.getTime() + (res.data.expiresIn * 1000)); 
          localStorage.setItem('token',res.data.idToken);
          localStorage.setItem('expirationDate',expirationDate);
          localStorage.setItem('userId',res.data.userId);

          dispatch('setLogoutTimer',res.data.expiresIn);

        })
          .catch(error => console.log(error));
    },
    tryAutoLogin({commit}) {
      const token = localStorage.getItem('idToken');
      if ( token ) {
        return;
      }
      const expirationDate = localStorage.getItem('expirationDate');
      const now = new Date();
      if ( now >= expirationDate ) {
        return;
      }

      const userId = localStorage.getItem('userId');
      commit('AUTH_USER',{token: token, userId: userId});

    },
    logout({commit}){
      commit('CLEAR_AUTH_DATA');
      localStorage.removeItem('token');
      localStorage.removeItem('expirationDate');
      localStorage.removeItem('userId');
      router.replace('/signin');
    },
    storeUser({commit,state},userData) {
      if ( !state.idToken ) {
        return;
      }

      globalAxios.post('/users.json?auth=' + state.idToken,userData)
        .then(res => {
          console.log('Store User');
          console.log(res);
        })
        .catch(error => console.log());
    },
    fetchUser({commit,state}) {
      if ( !state.idToken ) {
        return;
      }
      globalAxios.get('/users.json?auth=' + state.idToken)
      .then(res => {
        const data = res.data;
        const users = [];
        for (let key in data) {
          const user = data[key];
          user.id = key;
          users.push(user);
        }
        console.log(users);
        commit('STORE_USER',users[0]);
      })
      .catch(error => console.log(error));
    }
  },
  getters: {
    user(state) {
      return state.user;
    },
    isAuthenticated(state) {
      return state.idToken !== null;
    }
  }
})